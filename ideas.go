package gene

// Doc generation
// http://beego.me/docs/advantage/docs.md
// https://github.com/yvasiyarov/swagger

// Code generation
// http://clipperhouse.github.io/gen/#List

// Configuration change management

// Host Pooling
// https://github.com/bitly/go-hostpool
// https://github.com/strava/go.serversets

// Throttling
// https://github.com/PuerkitoBio/throttled
